<div align="center">
  <img src="demo/spire-logo.png" width="400"/>
</div>

---

**Spire格式图像/视频数据管理、分析、可视化工具。**


客户端当前版本：**v5.1.7**，下载地址：[**Baidu Pan (Windows x86-64)**](https://pan.baidu.com/s/1ZXj_fiwBrb6vSeTa2tLgyw?pwd=3nqw).

1. 修复右侧列表上限50个目标的bug


支持[Segment-Anything-Model (SAM)](https://github.com/facebookresearch/segment-anything.git)，[Grounding DINO](https://github.com/IDEA-Research/GroundingDINO.git)标注，部署服务端请参考：

1. [服务端本地部署方法](https://www.wolai.com/rGgq3Fm4SLGPcwFsjrX7ig)
2. [SpireView客户端与服务端通讯协议](https://www.wolai.com/f1BeT23zZp7rTjRL8aZNUD)
3. [B站视频教程](https://space.bilibili.com/516880777?spm_id_from=333.1007.0.0)



## 1. 如何使用

![labeling](demo/labeling.jpg)

支持多种数据标注格式

<tr>
<td><img src="demo/bbox_labeling.gif" width="50%"></td>
<td><img src="demo/instance_labeling.gif" width="50%"></td>
</tr>

## 2. Spire目录结构

- 最简单的目录结构如下：
  
  <img src="demo/2022-08-25-15-27-16-image.png" title="" alt="" width="423">

- 允许目录结构的**嵌套**使用，以帮助构建大型数据集、视频数据集

- 构建视频数据集时，**每个视频片段**需要放在一个**独立的文件夹**下

- 一个具体的嵌套目录结构如下：

<img title="" src="demo/2022-08-25-15-35-38-image.png" alt="" width="431">

## 3. Spire数据格式（.json）

### 3.1 各字段说明

<img title="" src="demo/20221117000910.png" width="600">

### 3.2 某个json示例

```json
{
    "annos": [{
        "area": 277,
        "bbox": [552, 251, 24, 17],
        "category_name": "car",
        "segmentation": [
            [561, 253, 552, 263, 558, 266, 564, 268, 573, 266, 576, 260, 576, 254, 572, 251]
        ],
        "keypoint_type": "car_w4_points",
        "keypoints": [228, 208, 2, 376, 365, 2, 390, 309, 2, 277, 190, 2],
        "tracked_id": 1,
        "obj_attrs": {
            "truncation": 0,
            "occlusion": 0
        }
    }],
    "file_name": "IMG_000001.jpg",
    "height": 720,
    "width": 1280,
    "optical_flow": "IMG_000001_FLOW.png",
    "img_attrs": {
        "side-view": 0,
        "bird-view": 1,
        "high-alt": 1,
        "medium-alt": 0,
        "low-alt": 0,
        "daylight": 0,
        "night": 1
    }
}
```

---

- 其中`file_name`, `height`, `width`, `annos`[`bbox`, `category_name`, `area`]为**必填**。其他字段视情况而定

## 4. 在Spire格式标注数据上评估算法性能

参考 [EVALUATION_README.md](utils/evaluate/README.md)

## 5. Spire与MS-COCO格式的相互转换

- MS-COCO格式转化到Spire格式

```bash
python to-spire-annotation/coco_to_spire.py --coco-anno path_to_coco_json --coco-image-dir path_to_coco_image_dir --output-dir spire_annotation_dir
```

- Spire格式转换到MS-COCO格式

![convert](demo/convert.png)

## 6. 支持数据分析

![stat](demo/stat.png)
